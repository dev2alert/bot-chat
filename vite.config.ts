import {defineConfig} from "vite";
import {createHtmlPlugin} from "vite-plugin-html";
import react from "@vitejs/plugin-react";
import {VitePWA} from "vite-plugin-pwa";

export default defineConfig(() => {
    const devMode: boolean = process.env.NODE_ENV === "development";

    return {
        server: {
            host: "0.0.0.0"
        },
        build: {
            sourcemap: devMode,
            rollupOptions: {
                onwarn(warning, warn) {
                    if (warning.code === "MODULE_LEVEL_DIRECTIVE") {
                        return;
                    }

                    warn(warning);
                }
            }
        },
        plugins: [
            createHtmlPlugin({
                minify: true
            }),
            react(),
            VitePWA({
                registerType: "autoUpdate",
                injectRegister: "script",
                devOptions: {
                    enabled: true
                },
                manifest: {
                    name: "Bot Chat",
                    short_name: "Bot Chat",
                    description: "AI-based service",
                    theme_color: "#065ebc",
                    icons: [
                        {
                            src: "/icon-48x48.png",
                            sizes: "48x48",
                            type: "image/png"
                        },
                        {
                            src: "/icon-72x72.png",
                            sizes: "72x72",
                            type: "image/png"
                        },
                        {
                            src: "/icon-96x96.png",
                            sizes: "96x96",
                            type: "image/png"
                        },
                        {
                            src: "/icon-128x128.png",
                            sizes: "128x128",
                            type: "image/png"
                        },
                        {
                            src: "/icon-144x144.png",
                            sizes: "144x144",
                            type: "image/png"
                        },
                        {
                            src: "/icon-152x152.png",
                            sizes: "152x152",
                            type: "image/png"
                        },
                        {
                            src: "/icon-192x192.png",
                            sizes: "192x192",
                            type: "image/png"
                        },
                        {
                            src: "/icon-512x512.png",
                            sizes: "512x512",
                            type: "image/png"
                        }
                    ]
                }
            })
        ]
    };
});
