<p align="center">
    <img src="public/logo.png" width="200" />
</p>
<h1 align="center">Bot Chat</h1>
<p align="center">AI-based service.</p>

<h2>Installing</h2>
<p>Preliminary requirements:</p>
<ul>
    <li><a href="https://nodejs.org/">Node.js</a> >= <code>18.12.1</code></li>
    <li><a href="https://www.npmjs.com/">npm</a> >= <code>9.1.2</code></li>
    <li><a href="https://yarnpkg.com/">yarn</a> >= <code>1.22.19</code></li>
</ul>
<p>Installing dependencies:</p>
<pre>yarn</pre>

<h2>Starting & Building</h2>
<p>Start application to development mode:</p>
<pre>yarn start</pre>
<p>Build application to production:</p>
<pre>yarn build</pre>

<h2>ESLint & Prettier</h2>
<p>Linter check:</p>
<pre>yarn lint</pre>
<p>Linter fix:</p>
<pre>yarn lint:fix</pre>
<p>Format code (with Prettier):</p>
<pre>yarn format</pre>

<h2>License</h2>
<p><a href="./LICENSE">MIT</a></p>
