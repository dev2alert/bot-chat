import React from "react";
import {Chat} from "./Chat";
import {GlobalStyle} from "./GlobalStyle";

export function App(): React.ReactElement {
    return (
        <>
            <Chat />
            <GlobalStyle />
        </>
    );
}
