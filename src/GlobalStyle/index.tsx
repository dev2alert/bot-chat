import "normalize.css";
import {createGlobalStyle} from "styled-components";

export const GlobalStyle = createGlobalStyle`
    @font-face {
        font-family: "SFProText Regular";
        src: url("/fonts/SFProText-Regular.ttf");
    }
    @font-face {
        font-family: "SFProText Heavy";
        src: url("/fonts/SFProText-Heavy.ttf");
    }
    @font-face {
        font-family: "OpenSans Regular";
        src: url("/fonts/OpenSans-Regular.ttf");
    }
    @font-face {
        font-family: "OpenSans Bold";
        src: url("/fonts/OpenSans-Bold.ttf");
    }
    @font-face {
        font-family: "Inter Regular";
        src: url("/fonts/Inter-Regular.ttf");
    }

    * {
        box-sizing: border-box;
    }
    *::selection {
        background: white;
        color: black;
    }
`;
