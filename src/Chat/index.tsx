import React, {useCallback, useState} from "react";
import {useAudio} from "react-use";
import {ChatDesc, ChatLayout, ChatTitle} from "./Layout";
import {ChatSendMessageField} from "./SendMessageField";
import {ChatMessages} from "./Messages";
import {ChatMessage} from "./Message";
import {Message} from "./types";
import {produce} from "immer";
import {nanoid} from "nanoid";
import notificationSound from "./sounds/notification.mp3";

export const Chat: React.FC = () => {
    const [messages, setMessages] = useState<Message[]>([]);
    // eslint-disable-next-line
    const [notificationSoundNode, _, notificationSoundControls] = useAudio({
        src: notificationSound
    });

    const handleSend = useCallback(
        (message: string) => {
            setMessages(
                produce((messages) => {
                    messages.push({
                        id: nanoid(),
                        name: "Artem",
                        avatar: null,
                        content: message,
                        isMy: true
                    });
                })
            );

            fetch(
                import.meta.env.MODE === "production"
                    ? "/proxy/api/v1/chat/send-message"
                    : "http://185.46.8.130/api/v1/chat/send-message",
                {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({message})
                }
            ).then(async (response) => {
                const stream: ReadableStream<Uint8Array> | null = response.body;

                if (stream !== null) {
                    const reader: ReadableStreamDefaultReader<Uint8Array> = stream.getReader();
                    const messageId: string = nanoid();

                    // eslint-disable-next-line
                    while (true) {
                        const {done, value} = await reader.read();

                        if (done && !value) {
                            break;
                        }

                        const chunksPlain: string = new TextDecoder("utf-8").decode(value, {
                            stream: true
                        });
                        const chunks: {status: "content" | "done"; value: string | null}[] =
                            chunksPlain
                                .split("}{")
                                .map((chunk) => {
                                    return (
                                        (chunk.charAt(0) !== "{" ? "{" : "") +
                                        chunk +
                                        (chunk.charAt(chunk.length - 1) !== "}" ? "}" : "")
                                    );
                                })
                                .map((chunk) => JSON.parse(chunk))
                                .filter(
                                    (chunk) => chunk.status === "content" && chunk.value !== null
                                );
                        const letters: string = chunks.map(({value}) => value).join("");

                        setMessages(
                            produce((messages) => {
                                const index: number = messages.findIndex(
                                    ({id}) => id === messageId
                                );
                                const message: Message | null = messages[index] ?? null;

                                if (message === null) {
                                    messages.push({
                                        id: messageId,
                                        name: "Bot",
                                        avatar: "/bot-avatar.png",
                                        content: letters,
                                        isMy: false
                                    });
                                    notificationSoundControls.seek(0);
                                    notificationSoundControls.play();
                                } else {
                                    message.content += letters;
                                }
                            })
                        );
                    }
                }
            });
        },
        [notificationSoundControls]
    );

    return (
        <>
            <ChatLayout
                title={<ChatTitle>Bot Chat</ChatTitle>}
                desc={<ChatDesc>AI-based service</ChatDesc>}
                messages={
                    <ChatMessages>
                        {messages.map(({id, name, avatar, content, isMy}) => (
                            <ChatMessage
                                key={id}
                                name={name}
                                avatar={avatar}
                                senderType={isMy ? "my" : "other"}
                            >
                                {content}
                            </ChatMessage>
                        ))}
                    </ChatMessages>
                }
                send={<ChatSendMessageField onSend={handleSend} />}
            />
            {notificationSoundNode}
        </>
    );
};
