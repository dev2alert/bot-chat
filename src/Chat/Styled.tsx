import {styled} from "styled-components";

export const ChatStyled = styled.div`
    display: flex;
    position: relative;
    width: 100vw;
    height: 100vh;
    background: linear-gradient(-140deg, #007afe, #003369);
`;

export const ChatBackground = styled.div`
    display: flex;
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    pointer-events: none;
`;

export const ChatContent = styled.div`
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    padding: 0px 20px;
    padding-bottom: 20px;
    overflow: auto;
`;

export const ChatContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    max-width: 780px;
    margin: auto;
`;

export const ChatHead = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    padding-top: 34px;
    @media (max-width: 870px) {
        padding-top: 20px;
    }
`;

export const ChatTitle = styled.h1`
    color: white;
    margin: 0px;
    line-height: 1.55;
    font-size: 58px;
    font-family: "OpenSans Bold";
    @media (max-width: 870px) {
        font-size: 38px;
    }
`;

export const ChatDesc = styled.p`
    color: white;
    margin: 0px;
    line-height: 1.55;
    font-size: 18px;
    font-family: "OpenSans Regular";
    @media (max-width: 870px) {
        font-size: 16px;
    }
`;

export const ChatBody = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    width: 100%;
`;
