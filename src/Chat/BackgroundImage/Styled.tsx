import {styled} from "styled-components";

export const ChatBackgroundImageStyled = styled.svg`
    position: fixed;
    right: -25%;
    width: 100%;
    height: 100%;
`;
