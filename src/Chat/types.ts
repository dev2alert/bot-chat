export interface Message {
    id: number | string;
    name: string;
    avatar: string | null;
    content: string;
    isMy: boolean;
}
