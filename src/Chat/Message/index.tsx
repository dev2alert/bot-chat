import React from "react";
import {
    ChatMessageAvatar,
    ChatMessageContainer,
    ChatMessageStyled,
    ChatMessageText
} from "./Styled";
import {ChatMessageSenderType} from "./types";

export interface ChatMessageProps extends React.PropsWithChildren {
    senderType: ChatMessageSenderType;
    name: string;
    avatar?: string | null;
}

export const ChatMessage: React.FC<ChatMessageProps> = ({
    senderType,
    name,
    avatar = null,
    children
}) => {
    return (
        <ChatMessageStyled $senderType={senderType}>
            <ChatMessageContainer $senderType={senderType}>
                {avatar !== null ? (
                    <ChatMessageAvatar as="img" src={avatar} alt={name} />
                ) : (
                    <ChatMessageAvatar>{name.charAt(0).toUpperCase()}</ChatMessageAvatar>
                )}
                <ChatMessageText $senderType={senderType}>{children}</ChatMessageText>
            </ChatMessageContainer>
        </ChatMessageStyled>
    );
};

export * from "./types";
