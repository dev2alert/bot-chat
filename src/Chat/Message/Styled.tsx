import {styled, css} from "styled-components";
import {ChatMessageSenderType} from "./types";

export const ChatMessageStyled = styled.div<{$senderType: ChatMessageSenderType}>`
    display: flex;
    width: 100%;
    ${({$senderType}) => {
        switch ($senderType) {
            case "my":
                return css`
                    justify-content: flex-end;
                `;
            default:
                return css`
                    justify-content: flex-start;
                `;
        }
    }}
`;

export const ChatMessageContainer = styled.div<{$senderType: ChatMessageSenderType}>`
    display: flex;
    gap: 13px;
    ${({$senderType}) => {
        switch ($senderType) {
            case "my":
                return css`
                    flex-direction: row-reverse;
                `;
            default:
                return css``;
        }
    }}
`;

export const ChatMessageAvatar = styled.span`
    display: inline-flex;
    flex-shrink: 0;
    align-items: center;
    justify-content: center;
    overflow: hidden;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    background: #d3e4fd;
    color: #065ebc;
    font-family: "SFProText Heavy";
    font-size: 18px;
`;

export const ChatMessageText = styled.p<{$senderType: ChatMessageSenderType}>`
    display: inline-flex;
    border-radius: 28px;
    overflow: hidden;
    padding: 12px 20px;
    line-height: 1.55;
    font-family: "SFProText Regular";
    font-size: 18px;
    max-width: 400px;
    margin: 0px;
    white-space: pre-wrap;
    ${({$senderType}) => {
        switch ($senderType) {
            case "my":
                return css`
                    color: white;
                    background: #007afe;
                `;
            default:
                return css`
                    color: black;
                    background: rgba(34, 118, 245, 0.2);
                    &::selection {
                        background: #007afe;
                        color: white;
                    }
                `;
        }
    }}
`;
