import React, {useCallback, useRef} from "react";
import {
    ChatSendMessageFieldButton,
    ChatSendMessageFieldInput,
    ChatSendMessageFieldStyled
} from "./Styled";
import {ChatSendMessageFieldIcon} from "./Icon";

export interface ChatSendMessageFieldProps {
    onSend?: (message: string) => unknown;
}

export const ChatSendMessageField: React.FC<ChatSendMessageFieldProps> = ({onSend}) => {
    const inputRef = useRef<HTMLInputElement>(null);

    const handleKeyDown = useCallback(
        (event: React.KeyboardEvent<HTMLInputElement>) => {
            if (event.key === "Enter") {
                const inputEl: HTMLInputElement | null = inputRef.current;

                if (inputEl !== null && inputEl.value !== "") {
                    onSend?.(inputEl.value);
                    inputEl.value = "";
                }
            }
        },
        [inputRef, onSend]
    );
    const handleButtonClick = useCallback(() => {
        const inputEl: HTMLInputElement | null = inputRef.current;

        if (inputEl !== null) {
            onSend?.(inputEl.value);
            inputEl.value = "";
        }
    }, [inputRef, onSend]);

    return (
        <ChatSendMessageFieldStyled>
            <ChatSendMessageFieldInput
                ref={inputRef}
                placeholder="Start typing here..."
                onKeyDown={handleKeyDown}
            />
            <ChatSendMessageFieldButton onClick={handleButtonClick}>
                <ChatSendMessageFieldIcon />
            </ChatSendMessageFieldButton>
        </ChatSendMessageFieldStyled>
    );
};
