import {styled} from "styled-components";

export const ChatSendMessageFieldStyled = styled.div`
    display: flex;
    width: 100%;
    margin-top: 38px;
    background: white;
    border-radius: 16px;
    overflow: hidden;
    @media (max-width: 870px) {
        margin-top: 24px;
    }
`;

export const ChatSendMessageFieldInput = styled.input`
    display: inline-flex;
    width: 100%;
    background: none;
    border: none;
    outline: none;
    padding: 15px;
    padding-left: 28px;
    padding-right: 0px;
    font-size: 20px;
    font-family: "Inter Regular";
    color: black;
    &::placeholder {
        color: #ced0d6;
    }
    &::selection {
        background: #007afe;
        color: white;
    }
`;

export const ChatSendMessageFieldButton = styled.button`
    display: inline-flex;
    justify-content: center;
    align-items: center;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    background: #007afe;
    border: none;
    margin: 16px;
    margin-left: 0px;
    overflow: hidden;
    cursor: pointer;
`;
