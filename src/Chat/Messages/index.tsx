import React, {useEffect, useRef} from "react";
import {
    ChatMessageList,
    ChatMessagesConnect,
    ChatMessagesConnectProgress,
    ChatMessagesConnectText,
    ChatMessagesEmpty,
    ChatMessagesEmptyText,
    ChatMessagesEnd,
    ChatMessagesScrollbar,
    ChatMessagesStyled
} from "./Styled";

export interface ChatMessagesProps extends React.PropsWithChildren {
    connect?: boolean;
}

export const ChatMessages: React.FC<ChatMessagesProps> = ({connect = false, children}) => {
    const endRef = useRef<HTMLDivElement>(null);
    const isEmpty: boolean = React.Children.toArray(children).length === 0;

    useEffect(() => {
        const endEl: HTMLDivElement | null = endRef.current;

        if (endEl !== null) {
            endEl.scrollIntoView({behavior: "smooth"});
        }
    }, [children]);

    return (
        <ChatMessagesStyled>
            {connect ? (
                <ChatMessagesConnect>
                    <ChatMessagesConnectProgress />
                    <ChatMessagesConnectText>Connection...</ChatMessagesConnectText>
                </ChatMessagesConnect>
            ) : isEmpty ? (
                <ChatMessagesEmpty>
                    <ChatMessagesEmptyText>
                        {`Message list is empty.\nSend first message so that bot can respond to you.`}
                    </ChatMessagesEmptyText>
                </ChatMessagesEmpty>
            ) : (
                <ChatMessagesScrollbar>
                    <ChatMessageList>{children}</ChatMessageList>
                    <ChatMessagesEnd ref={endRef} />
                </ChatMessagesScrollbar>
            )}
        </ChatMessagesStyled>
    );
};
