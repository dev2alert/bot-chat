import CircularProgress from "@mui/material/CircularProgress";
import {styled} from "styled-components";

export const ChatMessagesStyled = styled.div`
    display: flex;
    width: 100%;
    background: white;
    border-radius: 16px;
    padding: 36px 20px;
    padding-right: 6px;
    margin-top: 38px;
    @media (max-width: 870px) {
        margin-top: 24px;
    }
`;

export const ChatMessagesEnd = styled.div`
    pointer-events: none;
`;

export const ChatMessagesConnect = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    gap: 10px;
    width: 100%;
    height: 400px;
    @media (max-height: 800px) {
        height: 300px;
    }
    @media (max-height: 700px) {
        height: 200px;
    }
`;

export const ChatMessagesConnectProgress = styled(CircularProgress).attrs({
    style: {color: "#007afe"}
})``;

export const ChatMessagesConnectText = styled.p`
    color: black;
    font-size: 14px;
    font-family: "OpenSans Regular";
`;

export const ChatMessagesEmpty = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    width: 100%;
    height: 400px;
    @media (max-height: 800px) {
        height: 300px;
    }
    @media (max-height: 700px) {
        height: 200px;
    }
`;

export const ChatMessagesEmptyText = styled.p`
    color: black;
    font-size: 14px;
    line-height: 22px;
    font-family: "OpenSans Regular";
    white-space: pre-wrap;
    text-align: center;
`;

export const ChatMessagesScrollbar = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    width: 100%;
    height: 400px;
    overflow: auto;
    padding-right: 14px;
    &::-webkit-scrollbar {
        width: 8px;
    }
    &::-webkit-scrollbar-thumb {
        background: #007afe;
        border-radius: 4px;
    }
    @media (max-height: 800px) {
        height: 300px;
    }
    @media (max-height: 700px) {
        height: 200px;
    }
`;

export const ChatMessageList = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: flex-start;
    gap: 18px;
`;
