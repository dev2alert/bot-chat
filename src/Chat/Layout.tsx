import React from "react";
import {ChatBackground, ChatBody, ChatContainer, ChatContent, ChatHead, ChatStyled} from "./Styled";
import {ChatBackgroundImage} from "./BackgroundImage";

export interface ChatLayoutProps {
    title: React.ReactNode;
    desc: React.ReactNode;
    messages: React.ReactNode;
    send: React.ReactNode;
}

export const ChatLayout: React.FC<ChatLayoutProps> = ({title, desc, messages, send}) => {
    return (
        <ChatStyled>
            <ChatBackground>
                <ChatBackgroundImage />
            </ChatBackground>
            <ChatContent>
                <ChatContainer>
                    <ChatHead>
                        {title}
                        {desc}
                    </ChatHead>
                    <ChatBody>
                        {messages}
                        {send}
                    </ChatBody>
                </ChatContainer>
            </ChatContent>
        </ChatStyled>
    );
};

export * from "./Styled";
