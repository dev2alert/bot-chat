import "dotenv/config";
import express from "express";
import {join} from "path";
import process from "process";
import proxy from "express-http-proxy";

export const app = express();
export const appPath = process.cwd();
export const distPath = join(appPath, "./dist");
export const pagePath = join(distPath, "./index.html");

app.use("/proxy", proxy("185.46.8.130"));
app.use(express.static(distPath));
app.use((req, res) => res.sendFile(pagePath));
app.listen(49777);
